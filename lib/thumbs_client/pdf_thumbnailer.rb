require 'thumbs_client/errors'
require 'faraday'
require 'json/ext'

module ThumbsClient
  class PdfThumbnailer
    def create_thumbnails(filename: nil, thumbnail_prefix: nil, callback_url: ThumbsClient.configuration.default_callback_url)
      # Config errors
      raise NoAccessTokenError if ThumbsClient.configuration.access_token.nil?
      raise NoServiceUrlError if ThumbsClient.configuration.service_url.nil?
      # Missing arguments
      raise FilenameMissing if filename.nil?
      raise ThumbnailPrefixMissing if thumbnail_prefix.nil?
      raise CallbackUrlMissing if callback_url.nil?

      conn = Faraday.new(ThumbsClient.configuration.service_url)
      body = {
        type: "pdf",
        filename: filename,
        thumbnail_prefix: thumbnail_prefix,
        callback_url: callback_url
      }.to_json

      response = conn.post do |req|
        req.headers['Content-Type'] = 'application/json'
        req.headers['X-ACCESS-TOKEN'] = ThumbsClient.configuration.access_token
        req.body = body
      end

      raise AuthorizationError if response.status == 401
      return true if response.status == 201
      false
    end
  end
end
