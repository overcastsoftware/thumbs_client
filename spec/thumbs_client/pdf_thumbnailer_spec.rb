require 'spec_helper'

describe ThumbsClient::PdfThumbnailer do
  describe '#create_thumbnails' do
    before(:each) do
      ThumbsClient.configure do |config|
        config.access_token = 'top_secret'
        config.service_url = 'http://localhost:5001'
      end
    end

    describe 'validations' do
      it 'requires an access token' do
        ThumbsClient.configure do |config|
          config.access_token = nil
        end

        expect{
          subject.create_thumbnails(filename: 'overcast.test', callback_url: 'callback_url')
        }.to raise_error(NoAccessTokenError)
      end

      it 'requires a service URL' do
        ThumbsClient.configure do |config|
          config.service_url = nil
        end

        expect{
          subject.create_thumbnails(filename: 'overcast.test', callback_url: 'callback_url')
        }.to raise_error(NoServiceUrlError)
      end

      it 'requires a filename' do
        expect{
          subject.create_thumbnails(thumbnail_prefix: 'foothumb', callback_url: 'foo')
        }.to raise_error(FilenameMissing)
      end

      it 'requires a thumbnail prefix' do
        expect{
          subject.create_thumbnails(filename: 'overcast.test', callback_url: 'foo')
        }.to raise_error(ThumbnailPrefixMissing)
      end

      it 'requires a callback URL' do
        expect{
          subject.create_thumbnails(filename: 'overcast.test', thumbnail_prefix: 'foothumb')
        }.to raise_error(CallbackUrlMissing)
      end
    end

    it 'creates thumbnails' do
      subject.create_thumbnails(filename: 'overcast_test.pdf', thumbnail_prefix: 'foo_pdf_thumb', callback_url: 'callback_url')
    end
  end
end
