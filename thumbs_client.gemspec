# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'thumbs_client/version'

Gem::Specification.new do |spec|
  spec.name          = "thumbs_client"
  spec.version       = ThumbsClient::VERSION
  spec.authors       = ["Einar Jonsson"]
  spec.email         = ["einar@overcast.io"]
  spec.summary       = %q{A client library to integrate with the Thumbs service.}
  spec.description   = %q{}
  spec.homepage      = ""
  spec.license       = "MIT"

  spec.files         = `git ls-files -z`.split("\x0")
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.5"
  spec.add_development_dependency "rake"
  spec.add_development_dependency "rspec"

  spec.add_runtime_dependency "faraday", "~> 0.9.0"
end
