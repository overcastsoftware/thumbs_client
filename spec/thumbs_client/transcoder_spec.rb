require 'spec_helper'

describe ThumbsClient::Transcoder do
  describe '#transcode' do
    before(:each) do
      ThumbsClient.configure do |config|
        config.access_token = 'top_secret'
        config.service_url = 'http://localhost:5001'
      end
    end

    describe 'validations' do
      it 'requires an access token' do
        ThumbsClient.configure do |config|
          config.access_token = nil
        end

        expect{
          subject.transcode(filename: 'overcast.test', callback_url: 'callback_url')
        }.to raise_error(NoAccessTokenError)
      end

      it 'requires a service URL' do
        ThumbsClient.configure do |config|
          config.service_url = nil
        end

        expect{
          subject.transcode(filename: 'overcast.test', callback_url: 'callback_url')
        }.to raise_error(NoServiceUrlError)
      end

      it 'requires a filename' do
        expect{
          subject.transcode(output_key: 'foo', thumbnail_prefix: 'foothumb', callback_url: 'foo')
        }.to raise_error(FilenameMissing)
      end

      it 'requires a output key' do
        expect{
          subject.transcode(filename: 'overcast.test', thumbnail_prefix: 'foothumb', callback_url: 'foo')
        }.to raise_error(OutputKeyMissing)
      end

      it 'requires a thumbnail prefix' do
        expect{
          subject.transcode(filename: 'overcast.test', output_key: 'foo', callback_url: 'foo')
        }.to raise_error(ThumbnailPrefixMissing)
      end

      it 'requires a callback URL' do
        expect{
          subject.transcode(filename: 'overcast.test', output_key: 'foo', thumbnail_prefix: 'foothumb')
        }.to raise_error(CallbackUrlMissing)
      end
    end

    it 'transcodes' do
      subject.transcode(filename: 'overcast.test', output_key: 'foo', thumbnail_prefix: 'foothumb', callback_url: 'callback_url')
    end
  end
end
