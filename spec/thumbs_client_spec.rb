require 'spec_helper'

describe ThumbsClient do

  describe '.configure' do
    it 'accepts a default_callback_url parameter' do
      ThumbsClient.configure do |config|
        config.default_callback_url = 'http://www.foo.bar'
      end
      expect(ThumbsClient.configuration.default_callback_url).to eq 'http://www.foo.bar'
    end
  end

end
