class NoAccessTokenError < StandardError
end

class NoServiceUrlError < StandardError
end

class AuthorizationError < StandardError
end

class FilenameMissing < StandardError
end

class OutputKeyMissing < StandardError
end

class ThumbnailPrefixMissing < StandardError
end

class CallbackUrlMissing < StandardError
end
