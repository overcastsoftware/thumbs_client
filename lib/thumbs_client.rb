require "thumbs_client/version"
require 'thumbs_client/transcoder'
require 'thumbs_client/pdf_thumbnailer'

module ThumbsClient
  class << self
    attr_accessor :configuration
  end

  def self.configure
    self.configuration ||= Configuration.new
    yield(configuration)
  end

  class Configuration
    attr_accessor :service_url, :access_token, :default_callback_url
  end
end
